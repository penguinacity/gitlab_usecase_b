# gitlab-usecase-b
### What
Part A contains the rudimentary CRUD logic, this B part accesses the gitaly backed storage for more unique info.

### Why
In part A we identified basic operations on the repo. This will expand to per repo ops
like commit, history, source etc.

### Why use Elastic?
For any git application many different groups will have an emmense need for realtime data. There 
are many tools out there that provide static data ie the status of things at this moment. This data
is quickly old and useless. To have always current data requires more of an event model. By designing
the transform stage of our data pipe to seek out these events and amend the database in a useful
manner we always have complex answers immediately. The pipe is always on and the database in Elastic
is almost simultaneously current. We can trend and/or aggregate data old to this instant in a flash!

### Basic Gitlab info
Gitaly is a service that sits between gitlab and git allowing for complete redundancy.
It might even be said that this is a better design than GEO as it comes completely
transparent to the users. GRPC is well known to be faster.
Storage location on server: /var/opt/gitlab/git-data/repositories/@hashed...
@hashed is exposed in the gitaly logs from step A as 'grpc.request.glProjectPath'.
This in hand we can now examine per repo operations and history.

### Not just Gitlab 
I can and have architected similar designs for other git applications. The beef of the process is 
always different but the data in the database can be very similar. Challenge me to solve your most 
difficult problems using these products. Once your database is current the Kibana process can be
very powerful and exciting. Searches for answers at blinding speeds. Queries that solve other business 
units problems made simple. Dashboards that provide just the answers you need to solve complex issues
and identify trends and issues almost before they happen.
All these in an always on always available fashion.


more to come..

### Some Goals for Gitlab part B might include

```

- commit composition
- source code scan
- aggregation of undesirable elements
- check the deeper history for bad elements

```

Operation would leverage direct calls to libgit2. 
I will allot larger amounts of time to this if funding needs arise. If you like what I do ... fund me.
We can sit down and write an effective plan that works for your business model.
